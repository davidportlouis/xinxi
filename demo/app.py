import os
import flask
import numpy
import json
import time
import logging
import requests
#import pdftotext
from flask import Flask, request, render_template, Markup

logging.basicConfig(level=logging.ERROR)

app = Flask(__name__, template_folder="./templates/",
            static_folder="./static")

STATIC_PATH = "./static/uploads/"

@app.route("/apollon")
def index():
    return render_template("apollon.html", title="apollon")

@app.route("/artemis")
def ar_index():
    return render_template("artemis.html", title="artemis")

@app.route("/analyze_text", methods=["POST"])
def artemis():
    URL = "http://1aec38725219.ngrok.io/v1/summarizer/t5/"
    file = request.files["doc"]
    path = os.path.join(STATIC_PATH + file.filename)
    file.save(path)
    text = ""
    pdf = pdftotext.PDF(open(path, "rb"))
    for page in pdf:
        text += page
    preds = requests.get(URL+text)
    return preds.json()


@app.route("/analyze_senti", methods=["POST"])
def apollon():
    URL = "http://1aec38725219.ngrok.io/v1/sentiment/distilbert/"
    preds = requests.get(URL+request.form.get("sentence")).json()["response"]
    pos = preds["positive"]
    neg = preds["negative"]
    return render_template("senti.html", pos=pos, neg=neg)
    


if __name__ == "__main__":
    app.run(host='127.0.0.1', debug=False, threaded=True, port=8080)
