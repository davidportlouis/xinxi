import os
import torch
import flask
import numpy as np
import transformers
import werkzeug
werkzeug.cached_property = werkzeug.utils.cached_property
from flask import Flask, request, Markup
from flask_restplus import Api, Resource

app = Flask(__name__)
api = Api(app, version='1.0-beta', title="Xinxi")

# configs
T5_PATH = "./models/t5_small"
MAX_LEN = 512
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")


def t5_summarizer(model, text):

    tokenizer = transformers.T5Tokenizer.from_pretrained(T5_PATH)

    text = text.strip().replace('"', "").replace("\n", " ")
    text = "summarize: " + text
    o_len = len(text)
    inputs = tokenizer.encode_plus(text, None, add_special_tokens=True, max_length=512,
                                   return_token_type_ids=True, return_tensors="pt",
                                   truncation=True)

    ids = inputs["input_ids"]
    outputs = model.generate(input_ids=ids, max_length=50,
                             num_beams=4, early_stopping=True)
    preds = [tokenizer.decode(g, skip_special_tokens=True,
                              clean_up_tokenization_spaces=True) for g in outputs]
    preds = str(preds)[2:-2]
    s_len = len(preds)
    return preds, o_len, s_len


@api.route("/v1/summarizer/t5/<string:sentence>")
@api.doc(params={"sentence": "Text to be summarized"})
class Bert(Resource):
    def get(self, sentence):
        MODEL = transformers.T5ForConditionalGeneration.from_pretrained(T5_PATH)
        MODEL.eval()
        summary, o_len, s_len = t5_summarizer(MODEL, sentence)
        res = {}
        res["response"] = {
            "summary": summary,
            "original length": str(o_len),
            "summary length": str(s_len),
        }
        return flask.jsonify(res)


if __name__ == "__main__":
    app.run(host="0.0.0.0")
