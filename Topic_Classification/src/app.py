#! /bin/python3

import os
import torch
import flask
import numpy as np
import transformers
import werkzeug
import json
werkzeug.cached_property = werkzeug.utils.cached_property
from flask import Flask, request, Markup
from flask_restplus import Api, Resource

app = Flask(__name__)
api = Api(app, version='1.0-beta', title="Xinxi")

# configs
DISTIL_PATH = "./models/distilbert_base_uncased/"
MAX_LEN = 64
MODEL = None
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")


def lookup(preds):
    config = None
    with open(DISTIL_PATH + "mapping.json") as f:
        config = json.load(f)
    return config[str(int(preds.item()))]


def distilbert_predict(model, input_):
    tokenizer = transformers.DistilBertTokenizerFast.from_pretrained(
        DISTIL_PATH)
    max_len = 512
    review = str(input_)
    review = " ".join(review.split())
    inputs = tokenizer.encode_plus(review, None, add_special_tokens=True,
                                   max_length=max_len, padding="max_length", truncation=True,
                                   return_tensors="pt")
    ids = inputs["input_ids"]
    mask = inputs["attention_mask"]

    outputs = model(input_ids=ids, argument_2=mask)
    _, preds = torch.max(outputs.data, dim=1)
    conf, pos = torch.max(torch.softmax(outputs, dim=1), dim=1)
    return lookup(preds), conf.item()


@api.route("/v1/classification/distilBert/<string:sentence>")
@api.doc(params={"sentence": "Sentence to be analysed for"})
class DistilBert(Resource):
    def get(self, sentence):
        MODEL = torch.jit.load(DISTIL_PATH + "distilbert_traced_qt.pt")
        MODEL.eval()
        class_, conf = distilbert_predict(MODEL, sentence)
        res = {}
        res["response"] = {
            "sentence": sentence,
            "class": class_,
            "confidence": conf
        }
        return flask.jsonify(res)


if __name__ == "__main__":
    app.run(port=5052)
